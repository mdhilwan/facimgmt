var Mn 			= require('backbone.marionette'),
	firebase    = require('firebase'),
	Radio 		= require('backbone.radio');

var firebaseRad = Radio.channel('firebaseRad');

require('Chance')

var chance = new Chance();

var fireConfig = {
    apiKey: "AIzaSyDH17SVKuhsxFyr2co8hJOfjghkpCi0qPE",
    authDomain: "facimgmt-84bbb.firebaseapp.com",
    databaseURL: "https://facimgmt-84bbb.firebaseio.com",
    projectId: "facimgmt-84bbb",
    storageBucket: "facimgmt-84bbb.appspot.com",
    messagingSenderId: "357029226025"
};

module.exports = Frb = Mn.Object.extend({

	initialize: function () {
		console.warn('initialize Frb');

		firebase.initializeApp(fireConfig);

		this.listenTo(firebaseRad, 'data:getFromPath', this.dataGetFromPath);

		firebaseRad.reply('user:detail', this.userGetDetail, this);
	},

	isUserLoggedIn: function () {
		firebase.auth().onAuthStateChanged(function (user) {
			if (user) {
				firebaseRad.trigger('user:login', true)
			} else {
				firebaseRad.trigger('user:login', false)
			}
		})
	},

	handleRegister: function (email, password) {
		var self = this;
		firebase.auth().createUserWithEmailAndPassword(email, password)
			.then(function (value) {
				firebaseRad.trigger('user:registration:success');

				var user = firebase.auth().currentUser

				email = user.email;
				uid = user.uid;

				self.handleCreateUsers(uid, email);
			})
			.catch(function(error) {
				var errorCode = error.code;
				var errorMessage = error.message;

		        if (errorCode == 'auth/weak-password') {
	          		alert('The password is too weak.');
	        	} else {
	          		alert(errorMessage);
	        	}

	        	console.log(error);
			});
	},

	handleLogin: function (email, password) {
		firebase.auth().signInWithEmailAndPassword(email, password)
			.then(function (value) {
				firebaseRad.trigger('user:login:success');
			})
			.catch(function(error) {
				var errorCode = error.code;
				var errorMessage = error.message;
				console.error(errorCode, errorMessage);
			});
	},	

	handleSignout: function () {
		firebase.auth().signOut().then(function() {
			firebaseRad.trigger('user:signout:success');
		}, function(error) {
			alert(error)
			console.log(error);
		});
	},

	handleNewFacility: function (newFacility, parentId) {
		firebase.database().ref('facilities/' + parentId + '/list/' + newFacility.id)
			.set(newFacility);
		this.increaseFacilityCount('facilities/' + parentId)
	},

	increaseFacilityCount: function (facilityParentId) {
		firebase.database().ref(facilityParentId + '/count').once('value', function (count) {
			var c = count.val();

			firebase.database().ref(facilityParentId + '/count').set(c + 1);
		})
	},

	handleNewBooking: function (newBooking, facilityId) {
		newBooking.facilityId = facilityId;

		firebase.database().ref('bookings/' + facilityId + '/list/' + chance.hash({length:28}))
			.set(newBooking);
	},

	handleCreateUsers: function (userId, email) {
		firebase.database().ref('users/' + userId)
			.set({
				email: email,
				verified: false,
				facilities: {
					meetingRoom: true
				}
			});
	},

	userGetDetail: function () {
		return firebase.auth().currentUser;	
	},

	dataGetFromPath: function (res) {
		var dir = res.path;
		var callback = res.callback;
		console.info(res);

		var val;
		firebase.database().ref(dir + '/').on('value', function (db) {
			console.warn('data loaded');
			firebaseRad.trigger(callback, {
				db: db.val(),
				directory: dir
			});
		})
	},

	HANDLE_CREATE_FACILITY: function () {
		var self = this;
		firebase.database().ref('facilities/' + chance.hash({length:28}))
			.set({
				value: "Meeting Room",
				url: "meeting-room",
				key: "meetingRoom",
				count: 0,
				max: 4,
				list: ""
			});	
	}
})