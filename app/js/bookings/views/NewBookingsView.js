var Bb 					= require('backbone'),
	Mn 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	moment 				= require('moment'),
	stickit 			= require('backbone.stickit'),
	FacilitiesFb;

var firebaseRad 		= Radio.channel('firebaseRad');

require('jquery.inputmask');
require('Chance');
require('bootstrap-datepicker');
require('bootstrap-timepicker');

var chance = new Chance();

var maxHrPerDay = 12,
	minBookingDur = 0.5,
	firstHr = 8,
	lastHr = 20;

module.exports = NewBookingsView = Mn.View.extend({
	template: require("../templates/newBookings.hbs"),

	ui: {
		'startDate' : '.start-date',
		'startTime' : '.start-time',
		'endDate' 	: '.end-date',
		'endTime' 	: '.end-time',
		'closeView'	: '.close-view',
		'duration'	: '.duration',
		'saveBtn' 	: '.save-new-facility',
		'cost'		: '.cost',
		'btnRules'	: '.btn-rules',
		'tableRules': '.well.rules'
	},

	events: {
		'click @ui.closeView' 	: 'onCloseView',
		'click @ui.saveBtn' 	: 'onSaveBooking',
		'click @ui.btnRules' 	: 'toggleRules',
	},

	bindings: {
		'.start-time'			: 'startTime',
		'.end-time'				: 'endTime',
		'.min-booking-dur'		: {
			observe: 'minBookingDur',
			onGet: function (val) {
				console.log(val);
				return val;
			},
			onSet: function (val) {
				console.warn(val)
				return val;
			}
		},
	},

	onCloseView: function () {
		$('.bootstrap-timepicker-widget').remove();
	},

	onBeforeRender: function () {
		this.model.set('eventName', chance.sentence({words: 5}));
	},

	onSaveBooking: function () {
		this.trigger('saveBooking', this.model);
	},

	onRender: function () {
		var self = this;
		var startDate = self.getUI('startDate');
		var startTime = self.getUI('startTime');
		var endDate = self.getUI('endDate');
		var endTime = self.getUI('endTime');

		var timepickerOpt = {
			snapToStep: true,
			minuteStep: 15
		};

		startDate.datepicker();
		endDate.datepicker();

		_.defer(function(){
			$(startTime).timepicker(timepickerOpt).on('changeTime.timepicker', function (e) {
				self.changeTimePickerHandler(e);
			});
			$(endTime).timepicker(timepickerOpt).on('changeTime.timepicker', function (e) {
				self.changeTimePickerHandler(e);
			});
		});

		self.stickit();
		self.changeTimePickerHandler();
	},

	changeTimePickerHandler: function (event) {
		var saveBtn 	= this.getUI('saveBtn'),
			validTime 	= this.isValidTime(event);

		if (validTime) {
			saveBtn.removeAttr('disabled');
			this.setValues(validTime);
		} else {
			saveBtn.prop('disabled', 'disabled');
		}
	},

	setValues: function (res) {

		this.model.set('duration', res.duration);
		this.model.set('startDate', res.startDate);
		this.model.set('startTime', res.startTime);
		this.model.set('endDate', res.endDate);
		this.model.set('endTime', res.endTime);
		this.model.set('minBookingDur', minBookingDur);

		var daysInMth = moment().daysInMonth();

		var durInput 		= this.getUI('duration'),
			ttlCostInput 	= this.getUI('cost'),
			ttlCost;		

		ttlCost = res.duration * Number(this.model.get('ratePerHr'));
		
		this.model.set('totalCost', ttlCost);

		ttlCost = '$' + ttlCost.formatMoney(2, '.', ',');

		durInput.val(res.duration);
		ttlCostInput.val(ttlCost);
	},

	toggleRules: function () {
		var btnRules = this.getUI('btnRules'),
			tableRules = this.getUI('tableRules');

		_.defer(function(){
			$(tableRules).slideToggle();
		});
	},

	isValidTime: function (event) {
		var startTime 		= this.getUI('startTime').val(),
			endTime 		= this.getUI('endTime').val(),
			startDate 		= this.getUI('startDate').val(),
			endDate 		= this.getUI('endDate').val();

		var start 			= moment(this.formTime(startTime));
		var end 	 		= moment(this.formTime(endTime));

		if (start.hour() < firstHr) {
			start.hour(firstHr);
			start.minutes(0);
			end.hour(firstHr + 1);

			if (event) {
				$(event.currentTarget).timepicker('hideWidget');
				alert('Start time cannot be earlier than ' + firstHr + ':00 AM');
			}

		}

		var dur 			= moment.duration(end.diff(start)).asHours();

		if (dur <= 0) {
			var prevAttr = this.model.previousAttributes();
			alert('Please ensure your total duration is not 0 hr')
			return {
				duration: prevAttr.duration,
				startDate: prevAttr.startDate,
				startTime: prevAttr.startTime,
				endDate: prevAttr.endDate,
				endTime: prevAttr.endTime
			}
		} else {
			return {
				duration: dur,
				startDate: startDate,
				startTime: start.format('LT'),
				endDate: endDate,
				endTime: end.format('LT'),
			};
		}
	},

	formTime: function (val) {
		var d = new Date(),
			s = val;
		var parts = s.match(/(\d+)\:(\d+) (\w+)/);
		var hours = /am/i.test(parts[3]) ? parseInt(parts[1], 10) : parseInt(parts[1], 10) == 12 ? 12 : parseInt(parts[1], 10) + 12,
			minutes = parseInt(parts[2], 10);

		d.setHours(hours, minutes,0,0);

		return d;
	}
});