var Mn 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio');

var firebaseRad 		= Radio.channel('firebaseRad');

module.exports = ProfileMainView = Mn.View.extend({
	template: require("../templates/profileMain.hbs"),

	ui: {
		signoutBtn: '.sign-out-btn'
	},

	events: {
		'click @ui.signoutBtn' : 'signout'
	},

	signout: function () {
		firebaseRad.trigger('user:signout');
	}

});