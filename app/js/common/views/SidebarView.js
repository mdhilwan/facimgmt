var Bb 					= require('backbone'),
	Mn 					= require('backbone.marionette'),
	ProfileMainView 	= require('./ProfileMainView'),
	NavNodeView 		= require('./NavNodeView'),
	Radio 				= require('backbone.radio');

var firebaseRad 		= Radio.channel('firebaseRad');

module.exports = SidebarView = Mn.View.extend({
	template: require("../templates/sidebar.hbs"),

	regions: {
		main: "#profile-main",
		menu: "#profile-menu"
	},

	initialize: function () {
		this.listenTo(firebaseRad, 'load:firebase', this.renderSideBar);
	},

	onBeforeRender: function () {
		var self = this;
		self.menuCollection = new Bb.Collection(BaseMenu);

		var cur;
		self.menuCollection.each(function(menu) {
			if (menu.get('item') == self.model.get('route').value) {
				cur = menu;
			}
			if (menu.get('childMenu')) {
				menu.get('childMenu').each(function(childMenuItem) {
					if (childMenuItem.get('item') == self.model.get('route').value) {
						cur = childMenuItem;
					}
				});
			} 
		});

		if (cur) {
			cur.set('active', true);
		}

		self.menuCollection.on('change', function () {
			if (self.isAttached()) {
				self.navView.render();
			}
		})
	},

	onRender: function () {
		var self = this;

		self.navView = new NavView({
			collection 	: self.menuCollection
		})

		self.showChildView('main', new ProfileMainView());
		self.showChildView('menu', self.navView);
	},

	renderSideBar: function (firebaseRef) {
		var self = this;

		_.each(firebaseRef, function(ref) {
			var sidebarRef = self.menuCollection.findWhere({key: 'facilities'});
			
			if (sidebarRef) {
				if (_.isUndefined(sidebarRef.get('childMenu').findWhere({ item: ref.value }))) {
					sidebarRef.get('childMenu').add({ 
						item: ref.value,
						key: ref.key,
						child: true
					});
				}
			}
		});
	}
});

var BaseMenu = [
	{ 
		item: 'Home',
		key: 'home',
		url: 'home',
		fa: 'fa-tachometer',
	}, {
		item: 'Facilities',
		key: 'facilities',
		type: 'list',
		fa: 'fa-building',
		childMenu: new Bb.Collection()
	// }, { 
	// 	item: 'Vehicles',
	// 	key: 'vehicles',
	// 	type: 'vehicle',
	// 	fa: 'fa-car',
	// 	childMenu: new Bb.Collection()
	}, { 
		item: 'Account Settings',
		key: 'accountSettings',
		fa: 'fa-user',
	}, { 
		item: 'Tasks',
		key: 'tasks',
		fa: 'fa-check-circle',
		childMenu: new Bb.Collection()
	}, { 
		item: 'Help',
		key: 'help',
		fa: 'fa-life-ring'
	}
]

var NavView = Mn.CollectionView.extend({
	tagName: "ul",
	className: "nav",
	childView: NavNodeView,
	childViewOptions: function (model, index) { 
		return { parent: this }
	}
});