var Bb 			= require('backbone'),
	Mn 			= require('backbone.marionette');

module.exports = NavNodeView = Mn.View.extend({
	tagName: 'li',

	template: require("../templates/leftMenuItem.hbs"),

	regions: {
		tree: {
			el: 'ul',
			replaceElement: true
		}
	},

	events: {
		"click @ui.link" : "setActive"
	},

	ui: {
		link: "a" 
	},

	onRender: function () {
		var self = this;
		var childMenu = self.model.get('childMenu');

		if (self.model.get('active')) {
			self.$el.addClass('active');

			if (self.options.parent) {
				_.defer(function(){
					$(self.options.parent.$el).parent().addClass('active');
					self.options.parent.$el.addClass('active');
				});
			}
		} else {
			self.$el.removeClass('active');
		}

		if (childMenu && childMenu.length) {

			var childMenuView = new ChildMenuView({
				collection: childMenu
			})

			self.showChildView('tree', childMenuView);

			if (self.model.get('active')) {
				childMenuView.$el.addClass('active');
			}
		}

		self.model.on('change', function () {
			self.render();
		})
	},

	setActive: function (e) {

		var self = this;

		self.model.collection.each(function (nav) {
			nav.set({ 'active': false });

			if (nav.get('childMenu')) {
				nav.get('childMenu').each(function (child) { 
					child.set({ 'active': false });
				})
			}
		})
		self.model.set({ 'active': true });
	}
});

var ChildMenuView = Mn.CollectionView.extend({
	tagName: "ul",
	className: "child-menu",
	childView: NavNodeView,
	childViewOptions: function (model, index) { 
		return { parent: this }
	}
});