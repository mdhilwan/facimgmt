var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	welcomeView 		= require('../../welcome/views/WelcomeView'),
	Facilities 			= require('../../facilities/views/FacilitiesView'),
	Tenants 			= require('../../tenants/views/TenantsView');

module.exports = HomeView = Mm.View.extend({
	template: require("../templates/home.hbs"),

	regions: {
		welcome :  "#welcome",
		facilities: {
			el: "#facilities",
			replaceElement: true
		},
		tenants: {
			el: "#tenants",
			replaceElement: true
		}
	},

	onRender: function () {
		this.showChildView('welcome', new WelcomeView({
			model: new Backbone.Model({
				welcomeNote: "Loading your facilities...",
				welcomeNoteSet: false
			})
		}));
		this.showChildView('facilities', new Facilities());
		this.showChildView('tenants', new Tenants());
	}
});