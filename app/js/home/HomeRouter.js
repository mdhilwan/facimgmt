var Mn = require('backbone.marionette');

module.exports = HomeRouter = Mn.AppRouter.extend({
    appRoutes: {
        "home" : "ShowHome"
    }
});