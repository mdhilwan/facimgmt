var Mn 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	HomeView			= require('./views/HomeView');

var appHistory 			= Radio.channel('appHistoryCh');

module.exports = HomeController = {
    ShowHome: function () {
    	var homeView = new HomeView();
    	appHistory.trigger('show:home', homeView);
    }
};