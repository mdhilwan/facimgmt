var Mn = require('backbone.marionette');

module.exports = LoginRouter = Mn.AppRouter.extend({
    appRoutes: {
        "login" : "ShowLogin",
    }
});