var Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	stickit 			= require('backbone.stickit');

var firebaseRad 		= Radio.channel('firebaseRad');

module.exports = LoginView = Mm.View.extend({
	template: require("../templates/login.hbs"),
	
	ui: {
		loginModal: "#login-modal",
		loginBtn: '.login-btn',
		userEmail: '.user-email',
		userPassword: '.user-pwd'
	},

	events: {
		'click @ui.loginBtn' : 'validateLogin'
	},

	bindings: {
		'.user-email' : 'email',
		'.user-pwd' : 'password',
	},

	onRender: function () {
		var loginModal = this.getUI('loginModal');
		_.defer(function(){
			loginModal.modal({
				keyboard: false,
				backdrop: 'static'
			})
		});

		var userEmail = this.getUI('userEmail');
		$(userEmail).inputmask("email");

		this.stickit()
	},

	hideModal: function () {
		var loginModal = this.getUI('loginModal');
		loginModal.modal('hide');
	},

	validateLogin: function () {
		var userPwd = this.getUI('userPassword').val();
		var userEmail = this.getUI('userEmail').val();
		var loginBtn = this.getUI('loginBtn');


		if (_.isEmpty(userPwd) || _.isEmpty(userEmail)) {
			alert('Please provide both your email and password')
		} else {
			loginBtn.button('loading');
			firebaseRad.trigger('user:login:handler', this.model)
		}
	}
})