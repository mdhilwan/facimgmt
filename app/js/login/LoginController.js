var Bb                  = require('backbone'),
	Mn 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	LoginView 			= require('./views/LoginView');

var appHistory 			= Radio.channel('appHistoryCh');

module.exports = LoginController = {
    ShowLogin: function () {
    	var loginView = new LoginView({
    		model: new Bb.Model({
    			email: '',
    			password: ''
    		})
    	});
    	appHistory.trigger('show:login', loginView);
    },
};