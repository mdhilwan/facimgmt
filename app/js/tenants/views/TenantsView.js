var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette');

module.exports = TenantsView = Mm.View.extend({
	template: require("../../tenants/templates/tenants.hbs"),

	className: "col-md-6",

	ui: {
		collapse: '.collapse'
	},
	
	onRender: function () {
		var collapsePanel = this.getUI('collapse');
		collapsePanel.collapse();
	}
});