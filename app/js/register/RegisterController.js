var Bb                  = require('backbone'),
	Mn 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	RegisterView 		= require('./views/RegisterView'),
    RegisterModel       = require('./models/RegisterModel');

var appHistory 			= Radio.channel('appHistoryCh');

module.exports = RegisterController = {
    ShowRegister: function () {
    	var registerView = new RegisterView({
            model: new RegisterModel()
        });
    	appHistory.trigger('show:register', registerView);
    },
    ShowForgotPassword: function () {
    	console.info("ShowForgotPassword");
    }
};