var Mm 					= require('backbone.marionette'),
	stickit 			= require('backbone.stickit'),
	Radio 				= require('backbone.radio');

var firebaseRad 		= Radio.channel('firebaseRad');

require('jquery.inputmask')

module.exports = RegisterView = Mm.View.extend({
	template: require("../templates/register.hbs"),
	
	ui: {
		emailInput 		: "#emailInput",
		passwordInput 	: "#passwordInput",
		cfmPasswordInput: "#cfmPasswordInput",
		btnRegister 	: ".btn-register",
	},

	bindings: {
		'#emailInput' 		: 'email',
		'#passwordInput'	: 'password',
		'#cfmPasswordInput'	: 'cfmpassword',
	},

	events: {

		"change @ui.emailInput" : "requiredThis",
		"blur @ui.emailInput" : "requiredThis",
		"focus @ui.emailInput" : "removeError",

		"change @ui.passwordInput" : "requiredThis",
		"blur @ui.passwordInput" : "requiredThis",
		"focus @ui.passwordInput" : "removeError",

		"change @ui.cfmPasswordInput" : "requiredThis",
		"blur @ui.cfmPasswordInput" : "matchPassword",
		"focus @ui.cfmPasswordInput" : "removeError",

		"click @ui.btnRegister" : "validateAccount"
	},

	onRender: function () {
		this.stickit();

		var emailInput = this.getUI('emailInput');	
		$(emailInput).inputmask("email");
	},

	removeError: function (e) {
		var input = $(e.currentTarget);
		input.parents('.form-group').removeClass('has-error');
	},

	requiredThis: function (e) {
		var input = $(e.currentTarget);
		var inputVal = input.val();
		var self = this;
		
		if (_.isEmpty(inputVal)) {
			input.parents('.form-group').addClass('has-error');
		} else {
			input.parents('.form-group').removeClass('has-error');
		}
	},

	matchPassword: function (e) {
		var pwd = this.model.get('password');	
		var cfmPwd = this.model.get('cfmpassword');
		var pwdInput = this.getUI('passwordInput');
		var cfmPwdInput = this.getUI('cfmPasswordInput');

		if (pwd != cfmPwd) {
			pwdInput.parents('.form-group').addClass('has-error');
			cfmPwdInput.parents('.form-group').addClass('has-error');
		} else {
			this.getUI('cfmPasswordInput').removeClass('has-error');
			this.getUI('passwordInput').removeClass('has-error');
		}
	},

	validateAccount: function () {
		var email = this.model.get('email');
		var password = this.model.get('password');
		var cfmpassword = this.model.get('cfmpassword');

		var registerBtn = this.getUI('btnRegister');

		if (password.length < 4 || cfmpassword.length < 4) {
			alert('Please enter a valid password');
			return false;
		}

		if (password != cfmpassword) {
			alert('Your passwords does not match');
			return false;
		}
		
		if (_.isEmpty(email) || _.isEmpty(password) || _.isEmpty(cfmpassword)) {
			console.error('registration form fail');	
			alert('Please ensure all fields are complete');		
		} else {
			console.log('registration pass');	
			registerBtn.button('loading');		
			firebaseRad.trigger('user:register', this.model)
		}
	}
})