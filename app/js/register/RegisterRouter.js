var Mn = require('backbone.marionette');

module.exports = RegisterRouter = Mn.AppRouter.extend({
    appRoutes: {
        "register" : "ShowRegister",
        "forgot-password" : "ShowForgotPassword",
    }
});