var $ 			= require('jquery'),
	Bb 			= require('backbone'),
	Mm 			= require('backbone.marionette');

module.exports = Root = Bb.Model.extend({
	defaults: {
		"route" : {
			// key: "dashboard 1", value: "Dashboard 1"
			key: "home", value: "Home"
			// key: "overview", value: "Overview"
		}
	}
});