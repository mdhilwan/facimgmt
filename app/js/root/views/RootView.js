var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Frb             	= require('../../frb/Frb'),
	SidebarView 		= require('../../common/views/SidebarView'),
	Root 				= require('../models/Root'),

	LoginRouter			= require('../../login/LoginRouter'),
	LoginCtrl			= require('../../login/LoginController'),

	HomeRouter			= require('../../home/HomeRouter'),
	HomeCtrl			= require('../../home/HomeController'),

	RegisterRouter		= require('../../register/RegisterRouter'),
	RegisterCtrl		= require('../../register/RegisterController'),
	FacilityRouter		= require('../../facilities/FacilityRouter'),
	FacilityCtrl		= require('../../facilities/FacilityController'),

	Radio 				= require('backbone.radio');

var appHistory 			= Radio.channel('appHistoryCh');
var firebaseRad 		= Radio.channel('firebaseRad');
var blockuiRad 			= Radio.channel('blockuiRad');

var FacilitiesFb;

require('block-ui');

// Radio.tuneIn('appHistoryCh')

module.exports = RootView = Mm.View.extend({
	template: require("../templates/root.hbs"),

	regions: {
		leftCol 		: "#left-col",
		rightCol 		: "#right-col",
		frb 			: "#frb",
		loginView 		: "#loginView",
		registerView 	: "#registerView"
	},

	initialize: function () {
		console.warn('initialize RootView');

		this.listenTo(appHistory, 'show:register', this.showRegister);
		this.listenTo(appHistory, 'show:login', this.showLogin);
		this.listenTo(appHistory, 'show:forgetPassword', this.showForgetPassword);
		this.listenTo(appHistory, 'show:home', this.showHome);
		this.listenTo(appHistory, 'show:facility:details', this.showFacilityDetails);

		this.listenTo(firebaseRad, 'user:login', this.userLogin);
		this.listenTo(firebaseRad, 'user:login:success', this.userLoginSuccess);
		this.listenTo(firebaseRad, 'user:login:handler', this.userLoginHandler);
		this.listenTo(firebaseRad, 'user:register', this.userRegister);
		this.listenTo(firebaseRad, 'user:signout', this.userSignout);
		this.listenTo(firebaseRad, 'user:signout:success', this.userSignoutSuccess);
		this.listenTo(firebaseRad, 'user:registration:success', this.userRegistrationSuccess);

		this.listenTo(firebaseRad, 'facility:new', this.saveNewFacility);
		this.listenTo(firebaseRad, 'booking:new', this.saveNewBooking);

		this.listenTo(blockuiRad, 'block', this.block);

		this.listenTo(firebaseRad, 'rootView:data:loaded', this.showContent);

		this.registerRouter = new RegisterRouter({ controller: RegisterCtrl });
		this.loginRouter 	= new LoginRouter({ controller: LoginCtrl });
	},

	onRender: function () {
		this.frb = new Frb();
		this.frb.isUserLoggedIn();

		Bb.history.start();
	},

	showRegister: function (registerView) {
		if (this.getRegion('loginView').hasView()) {
			var loginView = this.getRegion('loginView').currentView;
				loginView.hideModal();
		}
		this.showChildView('registerView', registerView);
	},

	showForgetPassword: function () {
		console.log("showForgetPassword");	
	},

	showLogin: function (loginView) {
		this.showChildView('loginView', loginView);
		Bb.history.navigate('#/login');
	},

	showHome: function (homeView) {
		this.showChildView('rightCol', homeView); 
	},

	showFacilityDetails: function (facilityDetailsView) {
		this.showChildView('rightCol', facilityDetailsView); 
	},

	showContent: function () {

		var self = this;
		self.homeRouter 		= new HomeRouter({ controller: HomeCtrl });
		self.facilityRouter 	= new FacilityRouter({ controller: FacilityCtrl });
		self.rootModel  		= new Root();	
		self.sidebarView 		= new SidebarView({ model: self.rootModel });
		self.showChildView('leftCol', self.sidebarView);

		var currentHash = window.location.hash;
		if (_.isEmpty(currentHash)) {
			Bb.history.navigate('#/home');	
		} else {
			Bb.history.navigate('#/');	
			Bb.history.navigate(currentHash);	
		}
	},

	saveNewFacility: function (newFacility, path) {
		this.frb.handleNewFacility(newFacility, path)
	},

	saveNewBooking: function (newBooking, path) {
		this.frb.handleNewBooking(newBooking, path)
	},

	userRegister: function (registerModel) {
		this.frb.handleRegister(registerModel.get('email'), registerModel.get('password'));
	},

	userLogin: function (loggedIn) {
		if (loggedIn) {
			firebaseRad.trigger('data:getFromPath', {
				path: 'facilities',
				callback: 'rootView:data:loaded'
			});
		} else if (window.location.hash != '#/register') {
			Bb.history.navigate('#/login');	
		}
	},

	userLoginHandler: function (loginModel) {
		this.frb.handleLogin(loginModel.get('email'), loginModel.get('password'));
	},

	userSignout: function () {
		var cfm = confirm('Confirm signing out of FaciMgmt?');
		if (cfm) {
			this.frb.handleSignout();
		}
	},

	userSignoutSuccess: function () {
		this.getRegion('rightCol').empty();
		this.getRegion('leftCol').empty();
	},

	userLoginSuccess: function () {
		var loginView = this.getRegion('loginView').currentView;
			loginView.hideModal();

		this.getRegion('registerView').empty();
		Bb.history.navigate('#/home');
	},

	userRegistrationSuccess: function () {
		this.getRegion('registerView').empty();
		Bb.history.navigate('#/home');
	},

	block: function (msg) {
		$.blockUI({
            message: msg,
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
	},
});