var Bb 				= require('backbone'),
	Mm 				= require('backbone.marionette'),
	Radio 			= require('backbone.radio');

var firebaseRad  	= Radio.channel('firebaseRad');

module.exports = WelcomeView = Mm.View.extend({
	template: require("../templates/welcome.hbs"),

	className: "col-md-12",

	ui: {
		welcomeNote: ".welcomeNote",
		createBtn: ".create-btn",
		manageBtn: ".manage-btn",
	},

	events: {
		'click @ui.createBtn' : 'createFacility'
	},

	initialize: function () {
		console.warn('initialize WelcomeView');

		this.listenTo(firebaseRad, 'welcomeView:data:loaded', this.renderWelcomeNote);
	},

	onRender: function () {
		var self = this;

		self.welcomeNote = self.getUI('welcomeNote');
		self.createBtn = self.getUI('createBtn');
		self.manageBtn = self.getUI('manageBtn');
		self.welcomeNote.fadeIn();

		firebaseRad.trigger('data:getFromPath', {
			path: 'facilities',
			callback: 'welcomeView:data:loaded'
		});
	},

	renderWelcomeNote: function (res) {

		var self = this;
		var fbdb 			= res.db
		var randAction 		= _.sample(['create', 'manage']);
		var emptyFaci 		= _.where(fbdb, {count: 0});
		var notEmptyFaci 	= _.reject(fbdb, function (ref) {
			return ref.count == 0;
		});

		randAction = 'create';

		if (randAction == 'create' && emptyFaci.length != 0) { //&& notEmptyFaci.length != 0) {
			if (emptyFaci.length == fbdb.length && emptyFaci.length != 0) {
				var welcomeNote = 'Seems like you have maxed out your account. Click here to upgrade your plan.'
			} else {
				var randFaci = _.sample(emptyFaci);
				var welcomeNote = 'You do not have any ' + randFaci.value.toLowerCase() + ' to manage. Start here to create your first ' + randFaci.value.toLowerCase() + '.'
			}
			
		} else {
			if (notEmptyFaci.length == 0) {
				var randFaci = _.sample(emptyFaci);
				var welcomeNote = 'You do not have any facility to manage. Start here to create your first facility.'
			} else {
				randAction = 'manage';
				var randFaci = _.sample(notEmptyFaci);
				var welcomeNote = 'Check your ' + randFaci.value.toLowerCase() + 's, manage its bookings and see how much it\'s revenue this month.'
			}
		}

		self.welcomeNote.fadeOut({
			complete: function () {
				self.model.set('welcomeNoteSet', true);
				self.model.set('welcomeNote', welcomeNote);
				self.model.set('action', randAction);
				if (randFaci) self.model.set('key', randFaci.key);
				if (randFaci) self.model.set('facility', randFaci.value);

				self.welcomeNote.html(welcomeNote);
				self.welcomeNote.fadeIn();

				if (self.model.get('action') == 'manage') {
					self.manageBtn.fadeIn();
				} else if (self.model.get('action') == 'create') {
					self.createBtn.fadeIn();
				}
			}
		});
	},
	createFacility: function (e) {
		var btn = $(e.currentTarget);
		var type = btn.attr('faci-type');
		firebaseRad.trigger('facility:create', type)
	}
});