var Bb 					= require('backbone'),
	Mn 					= require('backbone.marionette'),
	FacilityNodeView	= require('./FacilityNodeView'),
	Radio 				= require('backbone.radio');

var firebaseRad 		= Radio.channel('firebaseRad');

module.exports = FacilitiesView = Mn.View.extend({
	template: require("../templates/facilities.hbs"),
	
	className: "col-md-6",

	regions: {
		facilities: "#facilities",
		vehicles: "#vehicles",
		newFacilityContent: "#newFacility .modal-content",
		upgradeContent: "#upgrade .modal-content",
	},

	initialize: function () {
		console.warn('initialize FacilitiesView');	
		this.listenTo(firebaseRad, 'facilitiesView:data:loaded', this.showFacilities);
	},
	
	onRender: function () {
		firebaseRad.trigger('data:getFromPath', {
			path: 'facilities',
			callback: 'facilitiesView:data:loaded'
		});	
	},

	showFacilities: function (res) {
		var fbdb 			= res.db

		var fbCol = new Bb.Collection();
		_.each(fbdb, function(item) { fbCol.add(item);});

		this.showChildView('facilities', new FacilitiesListView({
			collection: fbCol,
			parent: this
		}))

		// self.showChildView('vehicles', new VehiclesListView({
		// 	collection: window.faciFb,
		// 	parent: self
		// }))
	}
});

var FacilitiesListView = Mn.CollectionView.extend({
	childView: FacilityNodeView,
	childViewOptions: function (model, index) { 
		return { 
			parent: this,
			added: false
		}
	},
});

var VehiclesListView = Mn.CollectionView.extend({
	childView: FacilityNodeView,
	childViewOptions: function (model, index) { 
		return { 
			parent: this,
			added: false
		}
	},
});