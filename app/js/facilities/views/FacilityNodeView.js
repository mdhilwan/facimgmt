var Bb 							= require('backbone'),
	Mn 							= require('backbone.marionette'),
	ChildFacilitiesView			= require('./ChildFacilitiesView'),
	ChildFacilitiesEmptyView	= require('./ChildFacilitiesEmptyView'),
	NewFacilityView 			= require('./NewFacilityView'),
	UpgradePlan 			  	= require('../../upgrade/views/UpgradeView'),
	Radio 						= require('backbone.radio');

var firebaseRad 		= Radio.channel('firebaseRad');

module.exports = FacilityNodeView = Mn.View.extend({
	template: require("../templates/facilityNodeView.hbs"),

	regions: {
		panelBody: ".panel-body"
	},

	events: {
		"click @ui.createNew" : "createNew",
		"click @ui.buyMore" : "createNew"
	},

	ui: {
		createNew: ".create-new",
		collapse: '.collapse',
		buyMore: '.buy-more',
	},

	initialize: function () {
		this.listenTo(firebaseRad, 'load:firebase', this.render);
		this.listenTo(firebaseRad, 'facility:create', this.createNewHelper);
	},

	onRender: function () {
		var self = this;

		var list = self.model.get('list') || [],
			count = self.model.get('count'),
			max = self.model.get('max');
		
		var listArray = _.map(list, function(li){ return li; });
		var listColl = new Bb.Collection(listArray)

		var childFaciList = new ChildFaciList({
			collection: listColl,
			model: new Bb.Model({
				value: self.model.get('value'),
				parentId: self.model.get('id'),
			})
		})
		
		self.showChildView('panelBody', childFaciList);

		var collapsePanel = self.getUI('collapse');
		collapsePanel.collapse();
		
		if (self.options.parent.collection.indexOf(self.model) == 0 || _.size(self.model.changed) > 0) {
			self.open();
		}

		var createNewBtn = self.getUI('createNew');
		var buyMoreBtn = self.getUI('buyMore');

		if (count == max) {
			createNewBtn.addClass('hidden');
			buyMoreBtn.removeClass('hidden');
		}
	},

	createNewHelper: function (type) {
		if (type == this.model.get('key')) {
			this.createNew();
			this.options.parent.options.parent.getRegion('newFacilityContent').$el.parents('.modal').modal("show");
		}
	},

	createNew: function () {
		var self = this;
		var type = 'list',
			c = this.model.get('count'),
			m = this.model.get('max');

		if (c < m) {

			var list 			= this.model.get('list') || [],
				firebaseId 		= this.model.get('id');

			var newFacilityView = new NewFacilityView({
				model: this.model,
				parentId: firebaseId,
				type: this.model.get('value')
			});

			newFacilityView.on('saveNew', function (newObj, newObjId, parentId) {
				self.options.parent.options.parent.getRegion('newFacilityContent').$el.parents('.modal').modal('hide');
				$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
				self.options.parent.options.parent.getRegion('newFacilityContent').detachView();
				// self.faciCount('increase');
				
				// var clonedList = _.clone(list);
				// clonedList[newObjId] = newObj;

				firebaseRad.trigger('facility:new', newObj, parentId);
				
				// self.model.set('list', clonedList);
			})

			self.options.parent.options.parent.getRegion('newFacilityContent').show(newFacilityView);
			self.options.parent.options.parent.getRegion('newFacilityContent').$el.parents('.modal-dialog').removeClass('modal-lg');

		} else {
			var upgradePlan = new UpgradePlan();
			this.options.parent.options.parent.getRegion('newFacilityContent').show(upgradePlan);
			self.options.parent.options.parent.getRegion('newFacilityContent').$el.parents('.modal-dialog').addClass('modal-lg');
		}
	},

	// faciCount: function (func) {
	// 	if (func == 'increase') {
	// 		var c = this.model.get('count');
	// 		++c;
	// 		this.model.set('count', c);
	// 	} else if (func == 'increase') {
	// 		var c = this.model.get('count');
	// 		--c;
	// 		this.model.set('count', c);
	// 	}
	// },

	open: function () {
		var collapsePanel = this.getUI('collapse');
		collapsePanel.removeAttr('style');
		collapsePanel.addClass('in');
	}
});

var ChildFaciList = Mn.CollectionView.extend({
	tagName: "div",
	className: "list-group",
	childView: ChildFacilitiesView,
	emptyView: ChildFacilitiesEmptyView,
	childViewOptions: function (model, index) { 
		return { parent: this }
	}
});