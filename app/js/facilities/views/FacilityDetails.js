var Bb 					= require('backbone'),
	Mn 					= require('backbone.marionette'),
	moment 				= require('moment'),
	Radio 				= require('backbone.radio'),
	NewBookingsView		= require('../../bookings/views/NewBookingsView'),
	stickit 			= require('backbone.stickit'),
	FacilitiesFb;

require('fullcalendar');
require('block-ui');
require('moment');

var firebaseRad 		= Radio.channel('firebaseRad');
var blockuiRad 			= Radio.channel('blockuiRad');

module.exports = FacilityDetails = Mn.View.extend({
	template: require("../templates/facilityDetails.hbs"),

	className: 'col-md-12',

	regions: {
		newBookings: "#newBookings .modal-content",
	},

	bindings: {
		"span.monthlyIncome" : {
			observe: "monthlyIncome",
			onGet: function (val) {
				if (val) {
					return parseInt(val).formatMoney(2, '.', ',');
				} else {
					return "0.00";
				}
			}
		},
		"span.yearlyIncome" : {
			observe: "yearlyIncome",
			onGet: function (val) {
				if (val) {
					return parseInt(val).formatMoney(2, '.', ',');
				} else {
					return "0.00";
				}
			}
		},
		"span.totalIncome" : {
			observe: "totalIncome",
			onGet: function (val) {
				if (val) {
					return parseInt(val).formatMoney(2, '.', ',');
				} else {
					return "0.00";
				}
			}
		},
		"span.stats" : {
			observe: "facilityId",
			updateMethod: "html",
			onGet: function (val) {
				return "<a class='stats-link' href='#/stats/" + val + "'> view stats </a>"
			}
		}
	},

	events: {
		"click @ui.editBtn" 	: "editFacility",
		"click @ui.deleteBtn" 	: "deleteFacility",
		"click @ui.lockBtn" 	: "lockFacility",
		"click @ui.bookBtn" 	: "bookFacility",
	},

	ui: {
		calendar 	: "#bookings",
		editBtn 	: ".edit-btn",
		bookBtn 	: ".book-btn",
		lockBtn 	: ".lock-btn",
		deleteBtn 	: ".delete-btn",
		newBookingsModal : '#newBookings'
	},

	initialize: function () {
		this.listenTo(firebaseRad, 'facilityDetail:data:loaded', this.showFacilityDetail);
		this.listenTo(firebaseRad, 'facilityBooking:data:loaded', this.showBookingList);

		firebaseRad.trigger('data:getFromPath', {
			path: 'facilities' + '/' + this.options.parentId + '/list/' + this.options.facilityId,
			callback: 'facilityDetail:data:loaded'
		});	
	},

	onRender: function () {
		var self = this;
		var calendar = this.getUI('calendar');
		
		if (self.model.get('name') && (self.model.get('costPerHr') || self.model.get('costPerDay') || self.model.get('costPerMth'))) {
			_.defer(function(){
				$(calendar).html('')

				self.calendar = $(calendar).fullCalendar({
					header: {
						left: 'title prev,next',
						center: '',
						right: 'today month,agendaWeek'
					},
					contentHeight: 600,
					defaultView: 'agendaWeek',
					selectable: true,
					scrollTime: "08:00:00",
					select: function (start, end, jsEvent, view) {
						self.createNewBooking(start, end, jsEvent, view);
					}, 
					editable: true,
					eventClick: function (event, jsEvent, view) {
						var newTitle = prompt("Enter a new title for this event", event.title);

						if (newTitle != null) {
							event.title = newTitle.trim() != "" ? newTitle : event.title;
							self.calendar.fullCalendar("updateEvent", event);
						}
					}
				});

				firebaseRad.trigger('data:getFromPath', {
					path: 'bookings' + '/' + self.options.facilityId + '/list/' ,
					callback: 'facilityBooking:data:loaded'
				});
			});
		
			$("html, body").stop().animate({
				scrollTop:0
			}, '500', 'swing');
		}

		self.model.set('facilityId', self.options.facilityId);

		self.stickit();
	},

	showFacilityDetail: function (res) {
		this.model.set('unit', res.db.unit);
		this.model.set('name', res.db.value);
		this.model.set('costPerHr', res.db.cost.perHr.formatMoney(2, '.', ','));
		this.model.set('costPerDay', res.db.cost.perDay.formatMoney(2, '.', ','));
		this.model.set('costPerMth', res.db.cost.perMth.formatMoney(2, '.', ','));
		this.render();
	},

	showBookingList: function (res) {
		setTimeout(function () { $.unblockUI(); }, 500);
		
		if (this.calendar) {
			this.calendar.fullCalendar('removeEvents');

			var bookings = res.db;
			if (_.size(bookings) > 0) this.setStats(bookings);

			var calBookings = _.map(bookings, function(booking, bookingId) {
				var start = moment(new Date(booking.startDate + ' ' + booking.startTime).toISOString());
				var end = moment(new Date(booking.endDate + ' ' + booking.endTime).toISOString());
				
				return {
					id: bookingId,
					title: booking.eventName,
					start: start,
					end: end
				}
			});

			this.calendar.fullCalendar('renderEvents', calBookings, true)
		}
	},

	setStats: function (bookings) {
		var thisMonth = moment().month();
		var thisYear = moment().year();
		var monthlyIncome = 0;
		var yearlyIncome = 0;
		var totalIncome = 0;

		_.each(bookings, function(booking){
			var bookingDate = moment(new Date(booking.startDate));
			if (bookingDate.month() == thisMonth) {
				monthlyIncome += booking.totalCost;
			}
			if (bookingDate.year() == thisYear) {
				yearlyIncome += booking.totalCost;
			}
			totalIncome += booking.totalCost;
		});

		this.model.set('monthlyIncome', monthlyIncome);
		this.model.set('yearlyIncome', yearlyIncome);
		this.model.set('totalIncome', totalIncome);
	},

	createNewBooking: function (start, end, jsEvent, view) {
		var self 		= this;
		var startL 		= start.format('L');
		var endL 		= end.format('L');
		var startLT 	= start.format('LT');
		var endLT 		= end.format('LT');
		var dur 		= moment.duration(end.diff(start)).asHours();
		var totalCost 	= dur * Number(self.model.get('cost'));
			totalCost 	= totalCost.formatMoney(2, '.', ',');

		var newBookingsView = new NewBookingsView({
			model: new Bb.Model({
				startDate: startL,
				startTime: startLT,
				endDate: endL,
				endTime: endLT,
				duration: dur,
				totalCost: totalCost,
				ratePerHr: self.model.get('costPerHr'),
				ratePerDay: self.model.get('costPerDay'),
				ratePerMth: self.model.get('costPerMth')
			})
		});

		newBookingsView.on('saveBooking', function (booking) {
			self.getUI('newBookingsModal').modal('hide');
			self.getRegion('newBookings').detachView();
			
			firebaseRad.trigger('booking:new', booking.toJSON(), self.options.facilityId);
			blockuiRad.trigger('block', 'Saving new booking...');
		})

		self.getRegion('newBookings').show(newBookingsView);
		self.getUI('newBookingsModal').modal('show');
	},

	editFacility: function (e) {
		console.log("editFacility");
	},

	bookFacility: function (e) {
		console.log("bookFacility");
	},

	lockFacility: function (e) {
		console.log("lockFacility");
	},

	deleteFacility: function (e) {
		console.log("deleteFacility");
	},
});