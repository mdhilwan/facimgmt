var Bb 			= require('backbone'),
	Mn 			= require('backbone.marionette');

module.exports = ChildFacilitiesEmptyView = Mn.View.extend({
	tagName: "li",
	className: "list-group-item",
	template: require("../templates/childFacilitiesEmptyView.hbs"),
	onBeforeRender: function () {
		this.model.set('value', this.options.parent.model.get('value'));
	},
});