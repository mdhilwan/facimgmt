var Bb 			= require('backbone'),
	Mn 			= require('backbone.marionette');

module.exports = ChildFacilitiesView = Mn.View.extend({
	tagName: "a",

	className: "list-group-item",

	template: require("../templates/childFacilitiesView.hbs"),

	onRender: function () {
		this.$el.attr('href','#/view-detail/facility/' + this.options.parent.model.get('parentId') + '/' + this.model.get('id'));
	}
});