var Bb 					= require('backbone'),
	Mn 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	FacilitiesFb;

var firebaseRad 		= Radio.channel('firebaseRad');

require('jquery.inputmask'),
require('Chance');

var chance = new Chance();

module.exports = NewFacilityView = Mn.View.extend({
	template: require("../templates/newFacility.hbs"),

	ui: {
		saveNewFacility: 	".save-new-facility",
		facilityName: 		".facility-name",
		facilityCostPerHr: 	".facility-cost-hr",
		facilityCostPerDay: ".facility-cost-day",
		facilityCostPerMth: ".facility-cost-mth",
		facilityUnit: 		".facility-unit",
	},

	events: {
		"click @ui.saveNewFacility" : "onSaveNewFacility",
		"change @ui.facilityName" : "validateFacility",
		"change @ui.facilityCostPerHr" : "validateFacility",
		"change @ui.facilityCostPerDay" : "validateFacility",
		"change @ui.facilityCostPerMth" : "validateFacility",
		"change @ui.facilityUnit" : "validateFacility",
	},

	onBeforeRender: function () {
		this.model = new Bb.Model({
			faciName: chance.name(),
			faciCostPerHr: chance.floating({min: 0, max: 100, fixed: 2}),
			faciCostPerDay: chance.floating({min: 100, max: 500, fixed: 2}),
			faciCostPerMth: chance.floating({min: 1000, max: 5000, fixed: 2}),
		})
	},

	onRender: function () {
		var faciCostPerHrInput 	= this.getUI('facilityCostPerHr');	
		var faciCostPerDayInput = this.getUI('facilityCostPerDay');	
		var faciCostPerMthInput = this.getUI('facilityCostPerMth');	
		$(faciCostPerHrInput).inputmask("currency");
		$(faciCostPerDayInput).inputmask("currency");
		$(faciCostPerMthInput).inputmask("currency");

		// this.$el.find('.facility-unit option[value="' + this.model.get('faciUnit') + '"]').attr('selected', 'selected');
		this.validateFacility();
	},

	onSaveNewFacility: function () {
		var self = this;
		var faciName 		= self.getUI('facilityName').val();
		var faciCostPerHr 	= self.getUI('facilityCostPerHr').val();
		var faciCostPerDay 	= self.getUI('facilityCostPerDay').val();
		var faciCostPerMth 	= self.getUI('facilityCostPerMth').val();

		var userDetail = firebaseRad.request('user:detail');
		var usersObj = {};
		usersObj[userDetail.uid] = true;	

		var type = self.options.type;
		var id = chance.hash({length: 28});
		var newAsset = {
			bookings: "",
			cost: {
				perHr: self.cleanNumber(faciCostPerHr),
				perDay: self.cleanNumber(faciCostPerDay),
				perMth: self.cleanNumber(faciCostPerMth)
			},
			key: self.toCamelCase(faciName),
			users: usersObj,
			value: faciName,
			id: id
		}

		self.trigger('saveNew', newAsset, id, self.options.parentId);
	},

	validateFacility: function () {
		var faciName = this.getUI('facilityName').val();
		var faciCostPerHr = this.getUI('facilityCostPerHr').val();
		var faciCostPerDay = this.getUI('facilityCostPerDay').val();
		var faciCostPerMth = this.getUI('facilityCostPerMth').val();
		var saveFaci = this.getUI('saveNewFacility');

		if (!_.isEmpty(faciName) && (!_.isEmpty(faciCostPerHr)) || !_.isEmpty(faciCostPerDay) || !_.isEmpty(faciCostPerMth)) {
			saveFaci.removeAttr('disabled');
		} else {
			saveFaci.prop('disabled', 'disabled')
		}
	},

	cleanNumber: function (num) {
		return Number(num.replace(/[^0-9\.]+/g,""))
	},

	toCamelCase: function (str) {
		return str
	        .replace(/\s(.)/g, function($1) { return $1.toUpperCase(); })
	        .replace(/\s/g, '')
	        .replace(/^(.)/, function($1) { return $1.toLowerCase(); });	
	}
});