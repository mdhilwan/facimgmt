var Bb                  = require('backbone'),
    Mn 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	FacilityDetails		= require('./views/FacilityDetails');

var appHistory 			= Radio.channel('appHistoryCh');

module.exports = FacilityController = {
    ListFacility: function (type) {
    	console.log(type);
    },
    ViewDetailFacility: function (parentId, childId) {
    	var facilityDetails = new FacilityDetails({
            facilityId: childId,
    		parentId: parentId,
            model: new Bb.Model()
    	})
    	appHistory.trigger('show:facility:details', facilityDetails);
    }
};