var Bb 				= window.Backbone,
	Mn 				= require('backbone.marionette');

module.exports = FacilitiesList = Bb.Firebase.Collection.extend({
	url: 'https://facimgmt-84bbb.firebaseio.com/facilities'
});