var Mn = require('backbone.marionette');

module.exports = FacilityRouter = Mn.AppRouter.extend({
    appRoutes: {
        "view-list/facility/:type" 					: "ListFacility",
        "view-detail/facility/:parentId/:childId" 	: "ViewDetailFacility"
    }
});