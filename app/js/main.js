console.log("Facility Management")

window.jQuery = $ 	= require('jquery');
window._			= require('underscore');
window.Backbone		= require('backbone'),
window.Marionette	= require('backbone.marionette');

var RootView        = require('./root/views/RootView'),
    bootstrap       = require('bootstrap'),
    fullcalendar    = require('fullcalendar');

Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
        c = isNaN(c = Math.abs(c)) ? 2 : c, 
        d = d == undefined ? "." : d, 
        t = t == undefined ? "," : t, 
        s = n < 0 ? "-" : "", 
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
        j = (j = i.length) > 3 ? j % 3 : 0;
       return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

var FaciMgmt = Marionette.Application.extend({
	region: '#root',
	onStart: function () { 
		console.log("app started");
        this.showView(new RootView()); 
	}
})

var faciMgmt = new FaciMgmt();

faciMgmt.start();