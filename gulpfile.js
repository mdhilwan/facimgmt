// ////////////////////////////////////////////////
//
// EDIT CONFIG OBJECT BELOW !!!
// 
// jsConcatFiles => list of javascript files (in order) to concatenate
// buildFilesFoldersRemove => list of files to remove when running final build
// buildFolder => where the app will run from
// // //////////////////////////////////////////////

var config = {
	jsConcatFiles: [ 
		'./app/js/root/views/Root.js',
		'./app/js/app.js'
	], 
	buildFolder: './build'
};


// ////////////////////////////////////////////////
// Required taskes
// gulp build
// bulp build:serve
// // /////////////////////////////////////////////

var gulp 			= require('gulp'),
	sass 			= require('gulp-sass'),
	mainBowerFiles 	= require('main-bower-files'),
	sourcemaps 		= require('gulp-sourcemaps'),
	autoprefixer 	= require('gulp-autoprefixer'),
	browserSync 	= require('browser-sync'),
	browserify 		= require('browserify'),
	buffer 			= require('vinyl-buffer'),
	source 			= require('vinyl-source-stream'),
	hbsfy           = require("hbsfy"),
	runSequence 	= require('run-sequence'),
	reload 			= browserSync.reload,
	concat 			= require('gulp-concat'),
	gulpif 		 	= require('gulp-if'),
	argv 			= require('yargs').argv,
	uglify 			= require('gulp-uglify'),
	rename 			= require('gulp-rename'),
	util 			= require('gulp-util'),
	del 			= require('del');


// ////////////////////////////////////////////////
// Log Errors
// // /////////////////////////////////////////////

function errorlog(err){
	console.error(err.message);
	this.emit('end');
}

// ////////////////////////////////////////////////
// Browserify Tasks
// ///////////////////////////////////////////////
const LIBS = {
	"jquery":"jquery",
	"backbone":"backbone",
	"backbone.marionette":"backbone.marionette",
	"backbone.radio":"backbone.radio",
	"backbone.stickit":"backbone.stickit",
	"bootstrap":"bootstrap",
	"bootstrap-datepicker":"bootstrap-datepicker",
	"firebase":"firebase",
	"moment":"moment",
	"fullcalendar":"fullcalendar",
	"block-ui":"block-ui",
	"bootstrap-timepicker":"bootstrap-timepicker",
	"jquery.inputmask":"jquery.inputmask",
	"underscore":"underscore"
}
gulp.task('browserify', function () {
	var b = browserify({
        entries: './app/js/plugins.js',
        debug: false
    });

    Object.keys(LIBS).forEach(function (lib) {
        b.require(lib);
    });

	hbsfy.configure({
	    extensions: ['hbs']
	});

    return b.transform(hbsfy).bundle()
    	.pipe(source('./app/js/plugins.js'))
    	.pipe(buffer())
    	.pipe(gulpif(argv.prod, uglify()))
    	.pipe(concat('infra.min.js'))
    	.pipe(gulp.dest('./' + config.buildFolder + '/js/'))
    	.pipe(reload({stream:true}));
})

// ////////////////////////////////////////////////
// Scripts Tasks
// ///////////////////////////////////////////////

gulp.task('scripts', function() {
	var b = browserify({
        entries: './app/js/main.js',
        debug: true
    });
    return b.transform(hbsfy).bundle()
    	.on('error', function(err){
    		console.log(err)
            console.log('[BROWSERIFY ERROR] ' + err.message);
        })
        .pipe(source("main.js"))
        .pipe(buffer())
        .pipe(gulpif(argv.prod, uglify()))
        .on('error', util.log)
        .pipe(sourcemaps.init({loadMaps: true}))
		.pipe(concat('main.min.js'))		
		.pipe(sourcemaps.write('./'))
    	.pipe(gulp.dest('./' + config.buildFolder + '/js/'))
    	.pipe(reload({stream:true}));
});

// ////////////////////////////////////////////////
// Bower Tasks
// ///////////////////////////////////////////////

gulp.task('bower', function() {
	return gulp.src(mainBowerFiles('**/*.js'))
		.pipe(concat('vendor.js'))
    	.pipe(gulp.dest('./' + config.buildFolder + '/js/'))
    	.pipe(reload({stream:true}));
});

// ////////////////////////////////////////////////
// Styles Tasks
// ///////////////////////////////////////////////

gulp.task('styles', function() {
	gulp.src('app/scss/style.scss')
		.pipe(sourcemaps.init())
			.pipe(sass({outputStyle: 'compressed'}))
			.on('error', errorlog)
			.pipe(autoprefixer({
	            browsers: ['last 3 versions'],
	            cascade: false
	        }))	
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest( './'+ config.buildFolder + '/css'))
		.pipe(reload({stream:true}));
});


// ////////////////////////////////////////////////
// HTML Tasks
// // /////////////////////////////////////////////

gulp.task('html', function(){
    gulp.src('app/**/*.html')
    .pipe(gulp.dest(config.buildFolder))
    .pipe(reload({stream:true}));
});

// ////////////////////////////////////////////////
// Font Tasks
// // /////////////////////////////////////////////
// 
gulp.task('fonts', function() {
	return gulp.src('app/fonts/**')
		.pipe(gulp.dest('build/fonts'));
});

// ////////////////////////////////////////////////
// Browser-Sync Tasks
// // /////////////////////////////////////////////

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: config.buildFolder
        }
    });
});


// ////////////////////////////////////////////////
// Build Tasks
// // /////////////////////////////////////////////

// clean out all files and folders from build folder
gulp.task('build:cleanfolder', function (cb) {
	del([
		'build/**'
	], cb);
});

// task to create build directory of all files
gulp.task('build:copy', ['build:cleanfolder'], function(){
    return gulp.src('app/**/*/')
    .pipe(gulp.dest('build/'));
});

// task to removed unwanted build files
// list all files and directories here that you don't want included
gulp.task('build:remove', ['build:copy'], function (cb) {
	del(config.buildFilesFoldersRemove, cb);
});

gulp.task('build', ['build:copy', 'build:remove']);


// ////////////////////////////////////////////////
// Watch Tasks
// // /////////////////////////////////////////////

gulp.task ('watch', function(){
	gulp.watch('app/scss/**/*.scss', ['styles']);
	gulp.watch('app/**/*.hbs', ['scripts']);
	gulp.watch('app/js/**/*.js', ['scripts']);
  	gulp.watch('app/**/*.html', ['html']);
});


gulp.task('default', function (callback) {
	runSequence(
		'bower', [
			'scripts', 'browserify', 'styles', 'html', 'fonts'
		], 
		'browser-sync', 
		'watch'
	)
});




